<div class="container"
	style="box-shadow: 0px 0px 2px #ccc; background: #fff;">
	<div class="left-content" style="height: 700px;">
		<div class="latest-report-title">
			<h2>Search Results for <?php echo $name;?></h2>
		</div>
		<?php foreach($result as $item) {?>
	 <div class="latest-report-item">
			<h2><?php  echo $item->report_title;?></h2>
			<div class="cat-pub">
				<p>Description : <?php  echo substr($item->report_description,0,190);?></p>
			</div>
			<div class="report-buttons">
				<div class="read-news"><a href="<?php echo base_url().url_title($item->report_title,'-');?>.html">Read Full News</a></div>
			</div>
		</div>
		<?php } ?>
		 	
	</div>	 
	<div class="sidebar" style="height: 700px;">
		<div class="sidebar-box">
			<div class="sidebar-head">
				<h3>Latest Report</h3>
			</div>
			<?php foreach($latest as $report){ ?>
				<div class="sidebar-single-item">
					<?php echo $report['report_title']?>
				</div>
			<?php }?>
		</div>
		<div class="sidebar-box">
			<div class="sidebar-head">
				<h3>Upcoming Report</h3>
			</div>
			<?php foreach($upcoming as $report){ ?>
				<div class="sidebar-single-item">
					<?php echo $report['title']?>
				</div>
			<?php }?>
		</div>
	</div> 

</div>
<div style="clear: both"></div>
			