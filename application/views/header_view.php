<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/stylesheet.css" />
<title>Market</title>
</head>

<body>
	<div id="header-container">
		<div class="container">
			<div id="logo">
				<img src="<?php echo base_url();?>images/logo.png" width="241" height="67">
			</div>
			<div id="search-box">
			<form method="post" action="<?php base_url()?>searchReport">
				<input type="text" name="searchValue" placeholder="Search By Title or Any Keyword" />
				<div class="search-icon">
				<button type="submit"  class="search-icon"> <img src="<?php echo base_url();?>images/search-icon.png" width="21" height="26" /></button>
				</div>
			</form>
			</div>
		</div>
	</div>


	<div id="navigation-container">
		<div class="container">
			<div id="nav">
				<ul>
					<li><a href="<?php echo base_url();?>">Home</a></li>
					<li><a href="<?php echo base_url();?>categories">Categories</a>
						<ul>
							<?php foreach($categs as $categ){ ?>
								<li><a href="<?php base_url();?>getSubCategories?category=<?php echo $categ['parentCateg_name'];?>"><?php echo $categ['parentCateg_name'];?></a></li>
							<?php } ?>
						</ul>
					</li>
					<li><a href="<?php echo base_url();?>latest.html">Latest Reports</a></li>
					<li><a href="<?php echo base_url();?>news.html">News</a></li>
					<li><a href="<?php echo base_url();?>about-us.html">About Us</a></li>
					<li><a href="<?php echo base_url();?>contact.html">Contact Us</a></li>
				</ul>
			</div>
		</div>
	</div>