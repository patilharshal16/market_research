<?php
	//print_r($article); 
?>
<div class="container"
	style="box-shadow: 0px 0px 2px #ccc; background: #fff;">
	<div class="left-content" style="height: auto;">
		<div class="latest-report-title">
			<h2><?php echo $article['title'];?></h2>
		</div>
		<div class="about-us">
			<?php echo $article['content'];?>
		</div>
	</div>
	<div class="sidebar" style="height: 700px;">
		<div class="sidebar-box">
			<div class="sidebar-head">
				<h3>Latest Report</h3>
			</div>
			<?php $count = 0;?>
			<?php foreach($latest as $report){ ?>
				<?php if($count > 3) break;?>
				<div class="sidebar-single-item">
					<?php echo $report['report_title']?>
				</div>
				<?php $count++;?>
			<?php }?>
		</div>
		<div class="sidebar-box">
			<div class="sidebar-head">
				<h3>Upcoming Report</h3>
			</div>
			<?php foreach($upcoming as $report){ ?>
				<div class="sidebar-single-item">
					<?php echo $report['title']?>
				</div>
			<?php }?>
		</div>
	</div>
</div>

<div style="clear: both"></div>
