<div class="container"
	style="box-shadow: 0px 0px 2px #ccc; background: #fff;">
	<div class="left-content" style="height: 700px;">
		<div class="latest-report-title">
			<h2><i><?php echo $name;?></i></h2>
		</div>
		<?php foreach($result as $item) {?>
	 <div class="latest-report-item">
			<h2><?php  echo $item['categName'];?></h2>
			<div class="cat-pub">
			</div>
		</div>
		<?php } ?>
		 	
	</div>	 
	<div class="sidebar" style="height: 700px;">
		<div class="sidebar-box">
			<div class="sidebar-head">
				<h3>Latest Report</h3>
			</div>
			<?php foreach($latest as $report){ ?>
				<div class="sidebar-single-item">
					<?php echo $report['report_title']?>
				</div>
			<?php }?>
		</div>
		<div class="sidebar-box">
			<div class="sidebar-head">
				<h3>Upcoming Report</h3>
			</div>
			<?php foreach($upcoming as $report){ ?>
				<div class="sidebar-single-item">
					<?php echo $report['title']?>
				</div>
			<?php }?>
		</div>
	</div> 

</div>
<div style="clear: both"></div>
			