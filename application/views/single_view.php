<?php //$report = $data[0];?>

	<?php //print_r($data); die();?>

<div class="container" style="box-shadow:0px 0px 2px #ccc;background:#fff;">
	<div class="left-content">
	<div class="report-title">
		<h2><?php echo $data['report_title'];?></h2>
		<p>Category : <?php echo $data['categName'];?> <br>Published On : <?php echo $data['report_date'];?> </p>
	</div>
		<section class="tabs">
	            <input id="tab-1" type="radio" name="radio-set" class="tab-selector-1" checked="checked" />
		        <label for="tab-1" class="tab-label-1">Description</label>
		
	            <input id="tab-2" type="radio" name="radio-set" class="tab-selector-2" />
		        <label for="tab-2" class="tab-label-2">Table Of Content</label>
		
	            <input id="tab-3" type="radio" name="radio-set" class="tab-selector-3" />
		        <label for="tab-3" class="tab-label-3">Related Reports</label>
			
	            <input id="tab-4" type="radio" name="radio-set" class="tab-selector-4" />
		        <label for="tab-4" class="tab-label-4">Enquiry</label>
            
			    <div class="clear-shadow"></div>
				
		        <div class="content-tabs">
			        <div class="content-1 content-inner">
						<?php echo $data['report_description']; ?>
				    </div>
			        <div class="content-2 content-inner">
						<?php echo $data['table_of_contents']; ?>
					</div>
			        <div class="content-3 content-inner" id="rel-reports">
						<h2>Lorem ipsum dolor sit amet consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h2>
						<p>Category : Lorem Ipsum | Published Date : 2013-07-22</p>
						<h2>Lorem ipsum dolor sit amet consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h2>
						<p>Category : Lorem Ipsum | Published Date : 2013-07-22</p>
						<h2>Lorem ipsum dolor sit amet consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h2>
						<p>Category : Lorem Ipsum | Published Date : 2013-07-22</p>
						<h2>Lorem ipsum dolor sit amet consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h2>
						<p>Category : Lorem Ipsum | Published Date : 2013-07-22</p>
						<h2>Lorem ipsum dolor sit amet consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h2>
						<p>Category : Lorem Ipsum | Published Date : 2013-07-22</p>
						<h2>Lorem ipsum dolor sit amet consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h2>
						<p>Category : Lorem Ipsum | Published Date : 2013-07-22</p>
						<h2>Lorem ipsum dolor sit amet consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h2>
						<p>Category : Lorem Ipsum | Published Date : 2013-07-22</p>
						<h2>Lorem ipsum dolor sit amet consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</h2>
						<p>Category : Lorem Ipsum | Published Date : 2013-07-22</p>
					</div>
				    <div class="content-4 content-inner">
						<form>
							<table>
								<tr>
									<td>
										Name
									</td>
									<td>
										<input type="text" name="name" placeholder="Enter your name"/><br>
									</td>
								</tr>
								<tr>
									<td>
										Email ID
									</td>
									<td>
										<input type="email" name="email" placeholder="Enter your email ID"/><br>
									</td>
								</tr>
								<tr>
									<td>
										Phone Number
									</td>
									<td>
										<input type="phone" name="phno" placeholder="Enter your phone number"/><br>
									</td>
								</tr>
								
								<tr>
									<td>
										Message
									</td>
									<td>
										<textarea rows="8" cols="50" placeholder="Enter Your Message Here"></textarea>
									</td>
								</tr>
								
								<tr>
									<td>
									</td>
									<td>
										<input type="submit" name="submit" placeholder="Send"/><br>
									</td>
								</tr>
							</table>
						</form>
					</div>
		        </div>
			</section>		
	</div>
	<div class="sidebar" style="height: 700px;">
		<div class="sidebar-box">
			<div class="sidebar-head">
				<h3>Latest Report</h3>
			</div>
			<?php $count = 0;?>
			<?php foreach($latest as $report){ ?>
				<?php if($count > 3) break;?>
				<div class="sidebar-single-item">
					<?php echo $report['report_title']?>
				</div>
				<?php $count++;?>
			<?php }?>
		</div>
		<div class="sidebar-box">
			<div class="sidebar-head">
				<h3>Upcoming Report</h3>
			</div>
			<?php foreach($upcoming as $report){ ?>
				<div class="sidebar-single-item">
					<?php echo $report['title']?>
				</div>
			<?php }?>
		</div>
	</div>	
</div>

<div style="clear:both"></div>