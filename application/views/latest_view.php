
<div class="container" style="box-shadow:0px 0px 2px #ccc;background:#fff;">
	<div class="left-content" style="height:700px;">
		<div class="latest-report-title">
			<h2>Our Latest Reports</h2>
		</div>
		<?php foreach($latest as $report) {?>
		<div class="latest-report-item">
			<h2><?php echo $report['report_title']?></h2>
			<div class="cat-pub"><p>Category : <?php echo $report['categName'];?> | Published On : <?php echo $report['report_date'];?> </p></div>
			<div class="report-buttons">
				<div class="view-report">
					<a href="<?php echo base_url().url_title($report['report_title'],'-');?>.html">View Report</a>
				</div>
			</div>
		</div>
		<?php } ?>
		
		<div class="clear"></div>
		<div class="pagination">
			<div class="page-no"><?php echo $this->pagination->create_links();?> </div>
		</div>
	</div>
	<div class="sidebar" style="height: 700px;">
		<div class="sidebar-box">
			<div class="sidebar-head">
				<h3>Latest Report</h3>
			</div>
			<?php $count = 0;?>
			<?php foreach($reports as $report){ ?>
				<?php if($count > 3) break;?>
				<div class="sidebar-single-item">
					<?php echo $report['report_title']?>
				</div>
				<?php  $count++;?>
			<?php }?>
		</div>
		<div class="sidebar-box">
			<div class="sidebar-head">
				<h3>Upcoming Report</h3>
			</div>
			<?php foreach($upcoming as $report){ ?>
				<div class="sidebar-single-item">
					<?php echo $report['title']?>
				</div>
			<?php }?>
		</div>
	</div>	
</div>

<div style="clear:both"></div>