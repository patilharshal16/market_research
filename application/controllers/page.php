<?php
class Page extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('pagination');
		$this->load->model('categorymodel','categ');
		$this->load->model('reportmodel','report');
		$this->load->model('page_model','page');
	}
	
	function index(){
		header("Location: ".base_url());
	}
	
	function about(){
		
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		
		$latest = $this->report->getLatest(4);
		$upcoming = $this->report->getUpcomingReports(4);
		
		$about = $this->page->getAbout();
		$this->load->view('about_view',array('about'=>$about,'latest'=>$latest,'upcoming' => $upcoming));
		$this->load->view('footer_view');
		
	}
	
	function news(){
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		
		$latest = $this->report->getLatest(4);
		$upcoming = $this->report->getUpcomingReports(4);
		$this->load->view('news_view',array('latest'=>$latest,'upcoming' => $upcoming));
		//$this->load->view('sidebar_view');
		$this->load->view('footer_view');
	}
	
	function contact(){
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		
		$this->load->view('contact_view');
		$this->load->view('footer_view');
	}
	
	function latest(){
		/* echo "URI: ".$this->uri->segment(2); 
		die(); */
		$nomRows = $this->report->getTotalRows();
		$config['base_url'] = base_url().'latest.html';
		$config['total_rows'] = $nomRows;
		$config['per_page'] = 7;
		$config['num_links'] = 2;
		$config['uri_segment'] = 2;
		$config['use_page_numbers'] = TRUE;
		$config['num_tag_open'] ="<div class='page-no'>";
		$config['num_tag_close'] = "</div>";
		
		$this->pagination->initialize($config);
		
		$head = $this->categ->getCategs();
		//print_r($head);die();
		$this->load->view('header_view',array('categs'=>$head));
		$reports = $this->report->getLatest(4);
		$latest = $this->report->getPageReports($config['per_page'] = 7,$this->uri->segment(2));
	//	print_r($latest);die();
	
		$upcoming = $this->report->getUpcomingReports(4);
		$this->load->view('latest_view',array('reports' => $reports,'latest' => $latest,'upcoming' => $upcoming));
		$this->load->view('footer_view');
	}
}

