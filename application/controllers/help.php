<?php
class Help extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('categorymodel','categ');
		$this->load->model('reportmodel','report');
		$this->load->model('help_model','help');
	}
	
	function howToOrder(){
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		
		$latest = $this->report->getLatest(4);
		$upcoming = $this->report->getUpcomingReports(4);
		
		$data = $this->help->getHowToOrder();
		$this->load->view('page_view',array('latest'=>$latest,'article'=>$data,'upcoming'=>$upcoming));
		$this->load->view('footer_view');
	}
	
	function formatAndDelivery(){
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		
		$latest = $this->report->getLatest(4);
		$upcoming = $this->report->getUpcomingReports(4);
		
		$data = $this->help->getFormatAndDelivery();
		$this->load->view('page_view',array('latest'=>$latest,'article'=>$data,'upcoming'=>$upcoming));
		$this->load->view('footer_view');
	}
	
	function paymentOptions(){
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		
		$latest = $this->report->getLatest(4);
		$upcoming = $this->report->getUpcomingReports(4);
		
		$data = $this->help->getPaymentOptions();
		$this->load->view('page_view',array('latest'=>$latest,'article'=>$data,'upcoming'=>$upcoming));
		$this->load->view('footer_view');
	}
	
	function siteMap(){
		
	}
}