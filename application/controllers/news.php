<?php
class News extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('categorymodel','categ');
		$this->load->model('reportmodel','report');
		$this->load->model('news_model','news');
	}
	
	function index(){
		
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		
		$latest = $this->report->getLatest(4);
		$upcoming = $this->report->getUpcomingReports(4);
		$news = $this->news->getAll(7);
		$this->load->view('news_view',array('latest'=>$latest,'upcoming' => $upcoming,'news'=>$news));
		$this->load->view('footer_view');
	
	}
	
	function single($id=NULL,$slug=""){
		
		
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		
		$latest = $this->report->getLatest(4);
		$upcoming = $this->report->getUpcomingReports(4);
		$news = $this->news->getNewsById($id);
		$this->load->view('news_single_view',array('latest'=>$latest,'news'=>$news,'upcoming'=>$upcoming));
		
		$this->load->view('footer_view');
		
	}
	
	function view(){
		echo "hello";
	}
	
}