<?php
class Report extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->library('pagination');
		$this->load->model('categoryModel','categ');
		$this->load->model('reportmodel','report');
		$this->load->model('news_model','news');
		$this->load->model('route_model','data');
		
	}
	public function index(){
		
	}
	
	/* public function view($id=NULL,$slug="") {
		
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		
		if(!isset($id)){
			header("Location: ".base_url());
		}
		
		
		$latest = $this->report->getLatest(4);
		$upcoming = $this->report->getUpcomingReports(4);
		$reportData = $this->report->getReport($id);
		
		$this->load->view('single_view',array('data'=>$reportData,'latest'=>$latest,'upcoming'=>$upcoming));
		
		$this->load->view('footer_view');
	} */
	
	function view(){
		
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		$title = $this->uri->segment(1);
		//echo $title;
		//Get page details from model
		$page = $this->data->getPageDetailsByTitle($title);
		
		if(isset($page['report'])){
			$latest = $this->report->getLatest(4);
			$upcoming = $this->report->getUpcomingReports(4);
			$this->load->view('single_view',array('data'=>$page['report'][0],'latest'=>$latest,'upcoming'=>$upcoming));
		} else {
			//News
			$latest = $this->report->getLatest(4);
			$upcoming = $this->report->getUpcomingReports(4);
			//$this->load->view('single_view',array('data'=>$page['report'][0],'latest'=>$latest,'upcoming'=>$upcoming));
			$this->load->view('news_single_view',array('news'=>$page['news'][0],'latest'=>$latest,'upcoming'=>$upcoming));
		}
		
		//$this->report->
		$this->load->view('footer_view');
		
	}
	
	function searchReport(){
		$data = $this->report->reportSearch($_POST['searchValue']);
		$head = $this->categ->getCategs();
		$this->load->view('header_view',array('categs'=>$head));
		$latest = $this->report->getLatest(4);
		$upcoming = $this->report->getUpcomingReports(4);
		$this->load->view('list_view',array('result' => $data,'latest'=>$latest,'upcoming'=>$upcoming,'name' => $_POST['searchValue']));
		$this->load->view('footer_view');
	}
	
}