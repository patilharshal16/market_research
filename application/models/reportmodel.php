<?php
class reportmodel extends CI_Model{
	function getReports(){
		$this->db->select('*');
		$this->db->join('category','report.categId=category.categId' );
		$query = $this->db->get('report');
		$row = $query->result();
		return $row;
	}
	
	function getLatest(){
		$q = $this->db->get('report');
		$numRows = $q->num_rows();
		
		$this->db->select('*');
		//$this->db->limit($no);
		$this->db->limit(6);
		//$this->db->order_by("id", "desc");
		//$this->db->where('id > '.$val);
		$this->db->join('category','report.categId=category.categId' );
		$query = $this->db->get('report');
		$row = $query->result_array();
		return $row;
	}
	
	function getPageReports($perpage,$uri){
		$q = $this->db->get('report');
		$numRows = $q->num_rows();
		$this->db->select('*');
		$this->db->join('category','report.categId=category.categId' );
		$query = $this->db->get('report',$perpage,$uri);
		$row = $query->result_array();
		return $row;	
	}
	
	function getReportById($id) {
		$this->db->select('*');
		$this->db->where('id',$id);
		$this->db->join('category','report.categId=category.categId');
		$q = $this->db->get('report');
		
		if($q->num_rows){
			return $q->result_array();
		}
		return false; // Report with given ID doesn't exist
	}
	
	function getUpcomingReports($no=4){
		$q = $this->db->get('upcoming_reports');
		$numRows = $q->num_rows();
	
		//$val = $numRows-6;
	
		$this->db->select('*');
		$this->db->limit($no,$numRows-$no);
		//$this->db->order_by("id", "desc");
		//$this->db->where('id > '.$val);
		$this->db->join('category','upcoming_reports.category_id=category.categId' );
		$query = $this->db->get('upcoming_reports');

		/* echo $this->db->last_query();
		die(); */
				
		$row = $query->result_array();
		return $row;
	}
	
	function reportSearch($value){		
		$this->db->select('*');
		$this->db->like('report_title',$value);
		$this->db->join('category','report.categId=category.categId');
		$query = $this->db->get('report');
		$row = $query->result();
		return $row;
	}
	
	function getTotalRows(){
		$totalRows = $this->db->get('report');
		$result = $totalRows->num_rows();
		return $result;
	}
}