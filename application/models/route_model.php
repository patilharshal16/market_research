<?php
class Route_model extends CI_Model{
	function getPageDetailsByTitle($title){
		$this->db->select('*');
		$this->db->where('post_title',$title.'.html');
		$page = $this->db->get('routes');
		//echo $this->db->last_query();
		if($page->num_rows()>0){
			$page = $page->result_array();
			$page = $page[0];
			
			$type = $page['post_type'];
			$id = $page['post_id'];
			switch($type){
				case 1: //echo "report";
						//Get report details
						$report = $this->report->getReportById($id);
						return array('type'=>'report','report'=>$report);
						//print_r($report);
					break;
				case 2: //echo "news";
						$news = $this->news->getNewsById($id);
						return array('type'=>'news','news'=>$news);
					break;
			}
			
		}
	}
}