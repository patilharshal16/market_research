<?php
class Help extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('page_model','page');
		
	}
	
	function index(){
		$this->load->view('header_view');
		
		$help = $this->page->getHelp();
		
		$this->load->view('help_view',array('data'=>$help));
		$this->load->view('footer_view');
	}
	
	function save(){
		$this->page->saveHelp($_POST);
		header("Location: ".base_url()."help/");
	}
}