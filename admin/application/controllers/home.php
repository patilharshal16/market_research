<?php
class Home extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		//$this->load->database();
	}
	
	public function index(){
		$this->checkSession();
		$this->load->view('header_view');
		$this->load->view('footer_view');
	}
	
	function checkSession(){
		session_start();
		if(!isset($_SESSION['id'])){
			header("Location: ".base_url());
		}
	}
}