<?php
class News extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('news_model','news');
	}
	
	function index() {
		//List all news items
		$this->load->view('header_view');
		
		$news = $this->news->get();
		$this->load->view('list_news_view',array('news'=>$news));
		
		$this->load->view('footer_view');
	}
	
	function add() {
		//Display add news form
		
		$this->load->view('header_view');
		$this->load->view('add_news_view');
		$this->load->view('footer_view');
	}
	
	function addNews(){
		//Do DB operations to insert news
		$this->news->add($_POST);
		header("Location: ".base_url()."news");
	}
	
	function edit(){
		//Show screen to edit News
		$this->load->view('header_view');
		
		$id = $_GET['id'];
		$news = $this->news->getNews($id);
		$this->load->view('add_news_view',array('news'=>$news));
		
		$this->load->view('footer_view');
	}
	
	function update(){
		//Perform DB operations to UPDATE
		$this->news->edit($_POST);
		header("Location: ".base_url()."news");
	}
	
	function delete(){
		//Delete the News Item
		$id = $_GET['id'];
		$this->news->delete($id);
		header("Location: ".base_url()."news");
	}
}