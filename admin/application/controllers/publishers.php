<?php
class Publishers extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('publishers_model');
	}
	
	function index(){
		$this->load->view('header_view');
		
		$data = $this->publishers_model->getPublishers();
		$this->load->view('publishers_view',array('values' => $data));
		$this->load->view('footer_view');
	}
	
	function add(){
		$this->load->view('header_view');
		$this->load->view('add_publisher_view');
		$this->load->view('footer_view');
	}
	
	function addNew(){
		
		if (file_exists("uploads/" . $_FILES["publisher_logo"]["name"]))
		{
			echo $_FILES["publisher_logo"]["name"] . " already exists. ";
			die();
		}
		else
		{
			move_uploaded_file($_FILES["publisher_logo"]["tmp_name"],
			"uploads/" . $_FILES["publisher_logo"]["name"]);
			$file = "uploads/" . $_FILES["publisher_logo"]["name"];
		}
		$this->publishers_model->addPublisher($_POST['news_title'],$_POST['publisher_description'],$file);
	}
	
	function update(){
		
	}
}