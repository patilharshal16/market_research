<?php
class Category extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('categorymodel','categ');
		$this->load->model('parent_model','parent');
	}
	
	public function index(){
		$categs = $this->categ->getCategs();
		
		$this->load->view('header_view');
		$this->load->view('categList_view',array('categs' => $categs));
		$this->load->view('footer_view');
	}
	
	public function add(){
		//Get the parent categories
		$categs = $this->parent->getParentsCatgs();
		
		//Load the Views
		$this->load->view('header_view');
		$this->load->view('addCateg_view',array('categs' => $categs));
		$this->load->view('footer_view');
	}
	
	function addCateg(){
		$this->categ->addCategory($_POST);
		header("Location: ".base_url()."category");
	}
	
}