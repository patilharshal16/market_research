<?php
class Legal extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('page_model','page');
	}

	function index(){
		$this->load->view('header_view');

		$legal = $this->page->getLegal();

		$this->load->view('legal_view',array('data'=>$legal));
		$this->load->view('footer_view');
	}

	function save(){
		$this->page->saveLegal($_POST);
		header("Location: ".base_url()."legal/");
	}
}