<?php
class Login extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->model('user_model','user');
	}
	
	public function index(){
		$this->checkSession();
		
		$data = array('no_visible_elements' =>true);
		$this->load->view('header_view',$data);
		$this->load->view('login_view');
		$this->load->view('footer_view');		
	}
	
	public function authenticate(){
		$this->checkSession();
		
		if($this->user->authenticate($_POST)){
			header("Location: ".base_url()."home");
		} else {
			header("Location: ".base_url().'?login=fail');
		}
	}
	
	public function logout(){
		session_start();
		session_destroy();
		header("Location: ".base_url());
	}
	
	function checkSession(){
		session_start();
		if(isset($_SESSION['id'])){
			header("Location: ".base_url()."home");
		}
	}
}