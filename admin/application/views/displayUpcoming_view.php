<div id="content" class="span12">
	<!-- content starts -->
	<div class="box span12">
		<div class="box-header well" data-original-title="">
			<h2>
				<i class="icon-user"></i>Reports
			</h2>

		</div>
		<div class="box-content">
			<table
				class="table table-striped table-bordered bootstrap-datatable datatable">
				<thead>
					<tr>
						<th>Id</th>
						<th>Report Name</th>
						<th>Category</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<?php $i=1;?>
						<?php foreach ($data as $report){ ?>
						<td><?php echo $i++;?></td>
						<td><?php echo $report->title; ?></td>
						<!-- <td><?php echo $report->description;?></td> -->
						<td><?php echo $report->categName?></td>
						
						<td style="text-align:center;">
							<a href="<?php echo base_url();?>upcoming/edit?id=<?php echo $report->id;?>" title="Edit" data-rel="tooltip" class="btn btn-inverse"><i class="icon-edit icon-white"></i> </a>
						</td>
						<td style="text-align:center;">
							<a href="<?php echo base_url();?>upcoming/delete?id=<?php echo $report->id;?>" title="Delete" data-rel="tooltip" class="btn btn-danger"><i class="icon-trash icon-white"></i></a>
						</td>
					</tr>
					<?php }?>

				</tbody>
			</table>
		</div>
	</div>
	<!-- content ends -->
</div>