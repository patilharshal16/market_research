<?php
if(isset($report)){
	$report = $report[0];
} 
?>
<div id="content" class="span12">
	<!-- content starts -->
	<div class="box span12">
		<div class="box-header well" data-original-title="">
			<h2>
				<i class="icon-user"></i>Add Upcoming Reports
			</h2>

		</div>
		<div class="box-content">
			<form action="<?php echo isset($report)?base_url()."upcoming/update":base_url()."upcoming/addReport"; ?>" method="post"
				 enctype="multipart/form-data">
				<table class="table" id="addReport">
					<tr>
						<td>Report Name</td>
						<td>:</td>
						<td><input type="text" name="reportName" id="reportName" 
							value="<?php echo isset($report)?$report['title']:'';?>"
							placeholder="Enter Report Name" class="span3">
					
					</tr>
					<tr>
						<td>Description</td>
						<td>:</td>
						<td><textarea style="width: 35%; height: 100px;" name="description"
								id="description" placeholder="Enter Description" class="span3">
									<?php echo isset($report)?$report['description']:'';?>
								</textarea>
					
					</tr>
					
					<tr>
						<td>Table of Contents</td>
						<td>:</td>
						<td>
							<textarea style="width: 35%; height: 100px;" name="tableOfContents"
								id="tableOfContents" placeholder="Enter Table of Contents">
									<?php echo isset($report)?$report['table_of_contents']:'';?>
							</textarea>
						</td>
					</tr>
					
					<tr>
						<td>Report category</td>
						<td>:</td>
						<td>
							<select name="report_category" id="report_category"
								class="span3">
								<?php foreach ($data as $d){ ?>
									<option value="<?php echo $d->categId; ?>" <?php echo (isset($report) && $report['category_id']== $d->categId)?"selected":"";?>>
										<?php echo $d->categName; ?>
									</option>
								<?php }?>

							</select>
						</td>
					</tr>
					
					<tr>
						<td></td>
						<td></td>
						<td><input type="submit" value=
							"<?php echo (isset($report))?"Update Report":"Add Report";?>"
							class="btn btn-primary">
					
					</tr>
				</table>
				<?php if(isset($report)) {?>
					<input type="hidden" name="reportId" value="<?php echo $report['id'];?>" />
				<?php } ?>
			</form>
		</div>
	</div>
	<!-- content ends -->
</div>

