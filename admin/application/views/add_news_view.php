<?php
	if(isset($news)){
		$news = $news[0];
	}
?>
<div id="content" class="span12">
	<!-- content starts -->
	<div class="box span12">
		<div class="box-header well" data-original-title="">
			<h2>
				<i class="icon-user"></i>Add News
			</h2>

		</div>
		<div class="box-content">
			<form
				action="<?php echo isset($news)?base_url()."news/update":base_url()."news/addNews"; ?>"
				method="post" enctype="multipart/form-data">
				<table class="table" id="addNews">
					<tr>
						<td>News Title</td>
						<td>:</td>
						<td><input type="text" name="news_title" id="newsTitle"
							value="<?php echo isset($news)?$news['news_title']:'';?>"
							placeholder="Enter News Title" class="span3">
					
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td><textarea style="width: 35%; height: 200px;"
								name="news_content" id="news_content"
								placeholder="" class="span3">
									<?php echo isset($news)?$news['news_content']:'';?>
								</textarea>
					
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td><input type="submit"
							value="<?php echo (isset($news))?"Update News":"Add News";?>"
							class="btn btn-primary">
					
					</tr>
				</table>
				<?php if(isset($news)) {?>
				<input type="hidden" name="newsId"
					value="<?php echo $news['id'];?>" />
				<?php } ?>
			</form>
		</div>
	</div>
	<!-- content ends -->
</div>

