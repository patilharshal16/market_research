<div id="content" class="span12">
	<!-- content starts -->
	<div class="box span12">
		<div class="box-header well" data-original-title="">
			<h2>
				<i class="icon-user"></i>Reports
			</h2>

		</div>
		<div class="box-content">
			<table
				class="table table-striped table-bordered bootstrap-datatable datatable">
				<thead>
					<tr>
						<th>#</th>
						<th>News Title</th>
						<th>Edit</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
					<?php $i=1;?>
					<?php foreach($news as $item) {?>
					<tr>
						<td><?php echo $i++;?></td>
						<td><?php echo $item['news_title'];?></td>
						<td style="text-align:center;">
							<a href="<?php echo base_url();?>news/edit?id=<?php echo $item['id'];?>" title="Edit" data-rel="tooltip" class="btn btn-inverse"><i class="icon-edit icon-white"></i> </a>
						</td>
						<td style="text-align:center;">
							<a href="<?php echo base_url();?>news/delete?id=<?php echo $item['id'];?>" title="Delete" data-rel="tooltip" class="btn btn-danger"><i class="icon-trash icon-white"></i></a>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
	<!-- content ends -->
</div>