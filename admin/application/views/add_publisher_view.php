<div id="content" class="span12">
	<!-- content starts -->
	<div class="box span12">
		<div class="box-header well" data-original-title="">
			<h2>
				<i class="icon-user"></i>Add Publisher
			</h2>

		</div>
		<div class="box-content">
			<form
				action="<?php echo isset($pub)?base_url()."publishers/update":base_url()."publishers/addNew"; ?>"
				method="post" enctype="multipart/form-data">
				<table class="table" id="addPublisher">
					<tr>
						<td>Publisher Name</td>
						<td>:</td>
						<td><input type="text" name="news_title" id="newsTitle"
							value="<?php echo isset($pub)?$pub['publisher_name']:'';?>"
							placeholder="Enter Publisher Name" class="span3">
					
					</tr>
					<tr>
						<td>Description</td>
						<td>:</td>
						<td><textarea style="width: 35%; height: 200px;"
								name="publisher_description" id="publisher_description"
								placeholder="" class="span3">
									<?php echo isset($pub)?$pub['publisher_description']:'';?>
								</textarea>
					
					</tr>
					<tr>
						<td>Publisher Logo</td>
						<td>:</td>
						<td><input type="file" name="publisher_logo" id="publisher_logo"
							value="<?php echo isset($pub)?$pub['publisher_name']:'';?>"
							placeholder="Enter Publisher Name" class="span3">
					
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td><input type="submit"
							value="<?php echo (isset($pub))?"Update Publisher":"Add Publisher";?>"
							class="btn btn-primary">
					
					</tr>
				</table>
				<?php if(isset($news)) {?>
				<input type="hidden" name="newsId"
					value="<?php echo $news['id'];?>" />
				<?php } ?>
			</form>
		</div>
	</div>
	<!-- content ends -->
</div>