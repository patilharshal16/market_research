<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="utf-8">
	<title>Markets and Research</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- The styles -->
	<link id="bs-css" href="<?php echo base_url(); ?>/css/bootstrap-cerulean.css" rel="stylesheet">
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	</style>
	<link href="<?php echo base_url(); ?>/css/bootstrap-responsive.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>/css/charisma-app.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>/css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href='<?php echo base_url(); ?>/css/fullcalendar.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>/css/fullcalendar.print.css' rel='stylesheet'  media='print'>
	<link href='<?php echo base_url(); ?>/css/chosen.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>/css/uniform.default.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>/css/colorbox.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>/css/jquery.cleditor.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>/css/jquery.noty.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>/css/noty_theme_default.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>/css/elfinder.min.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>/css/elfinder.theme.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>/css/jquery.iphone.toggle.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>/css/opa-icons.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>/css/uploadify.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>/css/custom.css' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/htmlarea/style/jHtmlArea.css" />
    
    <!-- jQuery -->
	<script src="<?php echo base_url(); ?>/js/jquery-1.7.2.min.js"></script>

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- The fav icon -->
	<link rel="shortcut icon" href="img/favicon1.ico">
	
	<script type="text/javascript">
			$(document).ready(function(){
				$("textarea").htmlarea();
			});
	</script>
	<style>
		.jHtmlArea, .jHtmlArea iframe, .jHtmlArea .ToolBar{
			width: 100% !important;
		}
	</style>
	
		
</head>

<body>
	<?php if(!isset($no_visible_elements) || !$no_visible_elements)	{ ?>
	<!-- topbar starts -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="index.html"> <img alt="Logo" src="img/logo20.png" /> <span>Markets</span></a>
				
				
				
				<!-- user dropdown starts -->
				<div class="btn-group pull-right" >
					<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="icon-user"></i><span class="hidden-phone"> admin</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href="#">Profile</a></li>
						<li class="divider"></li>
						<li><a href="login.html">Logout</a></li>
					</ul>
				</div>
				<!-- user dropdown ends -->
				
				<div class="top-nav nav-collapse">
					<ul class="nav">
						<li><a href="#">Visit Site</a></li>
						<li>
							<form class="navbar-search pull-left">
								<input placeholder="Search" class="search-query span2" name="query" type="text">
							</form>
						</li>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</div>
	</div>
	<!-- topbar ends -->
	<?php } ?>
	<div class="container-fluid">
		<div class="row-fluid">
		<?php if(!isset($no_visible_elements) || !$no_visible_elements) { ?>
		
			<!-- left menu starts -->
			<div class="span2 main-menu-span">
				<div class="well nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li class="nav-header hidden-tablet">Main</li>
						<li><a class="ajax-link" href="<?php echo base_url();?>"><i class="icon-home"></i><span class="hidden-tablet"> Dashboard</span></a></li>
						<li class="nav-header hidden-tablet">Reports</li>
						<li><a class="ajax-link" href="<?php echo base_url();?>report"><i class="icon-eye-open"></i><span class="hidden-tablet"> List Reports</span></a></li>
						<li><a class="ajax-link" href="<?php echo base_url();?>report/add"><i class="icon-eye-open"></i><span class="hidden-tablet"> Add Report</span></a></li>
						<li><a class="ajax-link" href="<?php echo base_url();?>parentcategory"><i class="icon-edit"></i><span class="hidden-tablet">List Parent Categories</span></a></li>
						<li><a class="ajax-link" href="<?php echo base_url();?>parentcategory/add"><i class="icon-edit"></i><span class="hidden-tablet">Add Parent Category</span></a></li>
						<!-- <li class="nav-header hidden-tablet">Report Category</li> -->
						<li><a class="ajax-link" href="<?php echo base_url();?>category"><i class="icon-edit"></i><span class="hidden-tablet">List Categories</span></a></li>
						<li><a class="ajax-link" href="<?php echo base_url();?>category/add"><i class="icon-edit"></i><span class="hidden-tablet">Add Category</span></a></li>
						<!-- <li class="nav-header hidden-tablet">Upcoming Reports</li> -->
						<li><a class="ajax-link" href="<?php echo base_url();?>upcoming"><i class="icon-list-alt"></i><span class="hidden-tablet"> List Upcoming Reports</span></a></li>
						<li><a class="ajax-link" href="<?php echo base_url();?>upcoming/add"><i class="icon-list-alt"></i><span class="hidden-tablet"> Add Upcoming Reports</span></a></li>
						
						<li class="nav-header hidden-tablet">Publishers</li>
						<li><a class="ajax-link" href="<?php echo base_url();?>publishers"><i class="icon-list-alt"></i><span class="hidden-tablet"> List Publishers</span></a></li>
						<li><a class="ajax-link" href="<?php echo base_url();?>publishers/add"><i class="icon-list-alt"></i><span class="hidden-tablet"> Add Publisher</span></a></li>
						
						<li class="nav-header hidden-tablet">News</li>
						<li><a class="ajax-link" href="<?php echo base_url();?>news"><i class="icon-list-alt"></i><span class="hidden-tablet"> List News</span></a></li>
						<li><a class="ajax-link" href="<?php echo base_url();?>news/add"><i class="icon-list-alt"></i><span class="hidden-tablet"> Add News</span></a></li>
						
						<li class="nav-header hidden-tablet">Website Content</li>
						<li><a class="ajax-link" href="<?php echo site_url();?>/homecontent"><i class="icon-home"></i><span class="hidden-tablet"> Home Page</span></a></li>
						<li><a class="ajax-link" href="<?php echo site_url();?>/about"><i class="icon-edit"></i><span class="hidden-tablet"> About Page</span></a></li>
						
						<li class="nav-header hidden-tablet">Footer Content</li>
						<li><a class="ajax-link" href="<?php echo base_url();?>help"><i class="icon-home"></i><span class="hidden-tablet"> Help</span></a></li>
						<li><a class="ajax-link" href="<?php echo base_url();?>legal"><i class="icon-home"></i><span class="hidden-tablet"> Legal</span></a></li>
						<li><a class="ajax-link" href="<?php echo base_url();?>help"><i class="icon-home"></i><span class="hidden-tablet"> Connect With Us</span></a></li>
						<li><a class="ajax-link" href="<?php echo base_url();?>report"><i class="icon-home"></i><span class="hidden-tablet"> Find Report By</span></a></li>
						
					</ul>
					<!-- <label id="for-is-ajax" class="hidden-tablet" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label> -->
				</div><!--/.well -->
			</div><!--/span-->
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			<?php } ?>
