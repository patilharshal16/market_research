<div id="content" class="span12">
	<!-- content starts -->
	<div class="box span12">
		<div class="box-header well" data-original-title="">
			<h2>
				<i class="icon-user"></i>Add Category
			</h2>

		</div>
		<div class="box-content">
			<form action="<?php echo base_url();?>category/addCateg"
				method="POST">
				<fieldset>
					<legend>Add Category</legend>

					<label>Category name</label> <input type="text"
						placeholder="Category Name" name="categName"> <span
						class="help-block">Enter Category Name. Eg: Business & Finance,
						Energy & Transport, etc.</span>

					<?php if($categs != 0) {?>
					<label>Parent Category</label> <select name="parentCateg">
						<option value="0">--Select One--</option>
						<?php foreach($categs as $categ) {?>
						<option value="<?php echo $categ->parentCategId	;?>">
							<?php echo $categ->parentCateg_name; ?>
						</option>
						<?php } ?>
					</select> <span class="help-block">Select a parent category for the
						new category.</span>
					<?php } ?>

					<input type="submit" class="btn" value="Submit" />
				</fieldset>
			</form>

		</div>
	</div>
	<!-- content ends -->
</div>


