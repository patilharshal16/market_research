<div id="content" class="span12">
	<!-- content starts -->
	<div class="box span12">
		<div class="box-header well" data-original-title="">
			<h2>
				<i class="icon-user"></i>Parent Categories
			</h2>

		</div>
		<div class="box-content">

			<?php if($categs == 0) {?>
			<p class="lead well" align="center">
				No Parent categories added yet. You can add a New <a href="#">here</a>.
			</p>
			<?php } else {?>
			<table
				class="table table-striped table-bordered bootstrap-datatable datatable">
				<thead>
					<tr>
						<th>#</th>
						<th>Parent Category</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
					<?php $i=1; foreach ($categs as $parentCateg) {?>
					<tr>
						<td><?php echo $i++; ?></td>
						<td><?php echo $parentCateg->parentCateg_name; ?></td>
						<td style="text-align: center;"><a
							href="<?php echo base_url();?>parentcategory/delete?id=<?php echo $parentCateg->parentCategId;?>"
							title="Delete" data-rel="tooltip" class="btn btn-danger"><i
								class="icon-trash icon-white"></i> </a></td>
					</tr>
					<?php }?>
				</tbody>
			</table>
			<?php } ?>

		</div>
	</div>
	<!-- content ends -->
</div>

