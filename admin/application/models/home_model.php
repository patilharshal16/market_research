<?php
class Home_model extends CI_Model{
	function saveContent($data){
		
		//Delete the old data
		$this->db->empty_table('home');
		
		//Insert new data
		$data = array('title'=> $data['title'], 'content' => $data['homecontent']);
		$this->db->insert('home',$data);
		return;
	}
	
	function get(){
		$this->db->select('title,content');
		$this->db->from('home');
		$query = $this->db->get();
		if($query->num_rows() == 1){
			$result = $query->result_array();
			return $result[0];
		} 
		return '';
	}
}