<?php
class News_model extends CI_Model {
	function add($news){
		
		$data = array(
					'news_title' => $news['news_title'],
					'news_content' => $news['news_content']
				);
		$this->db->insert('news',$data);
		
		$id = $this->db->insert_id();
		
		//Insert into routes
		$route = array(
					'post_id' => $id,
					'post_type' => 2,
					'post_title' => url_title($news['news_title']).'.html'
				);
		$this->db->insert('routes',$route);
	}
	
	function edit($news){
		$data = array(
				'news_title' => $news['news_title'],
				'news_content' => $news['news_content']
		);
		$this->db->where('id',$news['newsId']);
		$this->db->update('news',$data);
		
		//Update the route in routes table
		$this->db->where('post_id',$news['newsId']);
		$this->db->where('post_type',2);
		$this->db->delete('routes');
		
		$route = array(
				'post_id' => $news['newsId'],
				'post_type' => 2,
				'post_title' => url_title($news['news_title']).'.html'
		);
		$this->db->insert('routes',$route);
	}
	
	function delete($id){
		$this->db->delete('news',array('id'=>$id));
		
		$this->db->where('post_id',$id);
		$this->db->where('post_type',2);
		$this->db->delete('routes');
		
	}
	
	function get(){
		$this->db->select('*');
		$q = $this->db->get('news');
		return $q->result_array();
		
	}
	
	function getNews($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		$q = $this->db->get('news');
		return $q->result_array();
	}
}