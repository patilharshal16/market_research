<?php
class Upcoming_model extends CI_Model{
	public function add($value){

		$data=array(
				'title' => $value['reportName'],
				'description' => $value['description'],
				'table_of_contents' => $value['tableOfContents'],
				'category_id' => $value['report_category']
		);
		
		$this->db->insert('upcoming_reports',$data);
		
	}
	
	function getReports(){
		$this->db->select('*');
		$this->db->join('category','upcoming_reports.category_id=category.categId' );
		$query = $this->db->get('upcoming_reports');
		$row = $query->result();
		return $row;
	}
	
	function editReport($value){
		$data=array(
				'title' => $value['reportName'],
				'description' => $value['description'],
				'table_of_contents' => $value['tableOfContents'],
				'category_id' => $value['report_category']
		);
	
		$this->db->where('id',$value['reportId']);
		$this->db->update('upcoming_reports',$data);
	
	}
	
	function delete($id){
		$this->db->where('id',$id);
		$this->db->delete('upcoming_reports');
	}
	
	function getReport($id){
		$this->db->select('*');
		$this->db->where('id',$id);
		$q = $this->db->get('upcoming_reports');
		$result = $q->result_array();
		return $result;
	
	}


}