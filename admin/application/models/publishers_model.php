<?php
class publishers_model extends CI_Model{
	
	function addPublisher($title,$description,$file){
		$data = array(
				'publisher_name' => $title,
				'publisher_description' => $description,
				'publisher_image' => $file
				);
		$result = $this->db->insert('publishers',$data);
		if(result){
			header("Location:".base_url()."publishers");
		}
	}
	
	function getPublishers(){
		$this->db->select('*');
		$q = $this->db->get('publishers');
		return $q->result_array();
	}
}