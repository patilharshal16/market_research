<?php
class About_model extends CI_Model{
	
	function saveAbout($about){
		$data = array(
					'type' => 3,
					'title' => 'About Us',
					'content' => $about,
					'url' => 'about-us.html'
				);
		
		$this->db->where('type','3');
		$this->db->where('url','about-us.html');
		$this->db->delete('pages');
		
		$this->db->insert('pages',$data);
		
	}
	
	function getAbout(){
		$this->db->where('type',3);
		$about = $this->db->get('pages');
		$about = $about->result_array();
		return $about[0];
	}
}
?>