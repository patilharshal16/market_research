<?php
class categoryModel extends CI_Model{
	function addCategory($value){
		$data = array(
				'categName' => $value['categName'],
				'parentCategId' => $value['parentCateg']
				);
		$this->db->insert('category',$data);
	}
	
	function getCategs(){
		$this->db->select('*');
		$this->db->join('parent_category','category.parentCategId =parent_category.parentCategId');
		$query = $this->db->get('category');
		
		if( $query->num_rows > 0 ){
			return $query->result();
		} else {
			return 0;
		}
	}
	
	/* function getCategsWithParents(){
		$this->db->select('*');
		$query = $this->db->get('category');
		
		if( $query->num_rows > 0 ){
			$categs = $query->result();
			
			$final = array();
			foreach($categs as $categ){
				$temp = array();
				
				$temp['categId'] = $categ->categId;
				$temp['categName'] = $categ->categName;
				$id = $temp['categId'];
				
				$this->db->select('*');
				$this->db->from('parent_category');
				$this->db->join('category', 'category.categId = parent_category.categParentId');
				$this->db->where('parent_category.categId',$id);
				$parents = $this->db->get();
				if($parents->num_rows > 0){
					$parents = $parents->result();
					$parent = $parents[0];
					$temp['categParentId'] = $parent->categParentId;
					$temp['categParent'] = $parent->categName;
				} else {
					$temp['categParentId'] = false;
					$temp['categParent'] = false;
				}
				$final[] = $temp;
			}
		} else {
			return 0;
		}
		 echo '<pre>';
		print_r($final);
		die(); 
	
		return $final;	
	} */
	
	function getcategory(){
		$query = $this->db->get('category');
		$row=$query->result();
		return $row;
	}
}